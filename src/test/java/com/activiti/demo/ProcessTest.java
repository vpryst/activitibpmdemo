package com.activiti.demo;

import static org.junit.Assert.assertEquals;

import org.activiti.engine.RuntimeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.activiti.demo.StartProcessBean;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/META-INF/spring/process-application-context.xml")
public class ProcessTest {

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private StartProcessBean startProcessBean;

	@Test
	public void simpleProcessTest() {
		try {
			startProcessBean.doTransactionalStuff(false);
		} catch (RuntimeException re) {
			System.out
					.println("Catching RuntimeException for testing purposes.." + re.getMessage() + " " + re.getStackTrace());
		}
		assertEquals(1, runtimeService.createProcessInstanceQuery().count());
	}
};
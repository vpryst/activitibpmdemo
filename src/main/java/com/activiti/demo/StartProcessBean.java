package com.activiti.demo;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.springframework.transaction.annotation.Transactional;

public class StartProcessBean {
	private RuntimeService runtimeService;

	@Transactional
	public void doTransactionalStuff(boolean error) {
		runtimeService.startProcessInstanceByKey("springTransactionTest");
	}

	public void setRuntimeService(RuntimeService runtimeService) {
		this.runtimeService = runtimeService;
	}
}
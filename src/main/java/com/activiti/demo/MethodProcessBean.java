package com.activiti.demo;

import org.activiti.engine.impl.pvm.delegate.ActivityExecution;


public class MethodProcessBean {
	public void doSimpleStuff(ActivityExecution exec) {
		System.out.println("Process Executed " + exec.getCurrentActivityId());
	}
}
